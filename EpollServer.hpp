/*
 * EEpoll.hpp
 *
 *  Created on: 20 июл. 2019 г.
 *      Author: avatar
 */

#ifndef EPOLLSERVER_HPP_
#define EPOLLSERVER_HPP_
#include "stdint.h"
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
/*
 * Поле events является битовой маской, составляемой из следующих возможных типов событий:

EPOLLIN
    Связанный файл доступен для чтения с помощью read(2).
EPOLLOUT
    Связанный файл доступен для записи с помощью write(2).
EPOLLRDHUP (начиная с Linux 2.6.17)
    Одна из сторон потокового сокета закрыла соединение, или выключила записывающую часть соединения. (Этот флаг особенно полезен при написании простого кода для обнаружения отключения стороны с помощью слежения Edge Triggered.)
EPOLLPRI
    Для операций read(2) есть срочные данные.
EPOLLERR
    Произошла ошибка со связанным файловым дескриптором. epoll_wait(2) всегда будет ждать этого события; его не нужно устанавливать в events.
EPOLLHUP
    Произошло зависание связанного файлового дескриптора. Вызов epoll_wait(2) будет всегда ждать этого события; его не нужно указывать в events. Заметим, что при чтении из канала, такого как канал (pipe) или потоковый сокет, это событие всего-навсего показывает, что партнёр закрыл канал со своего конца. Дальнейшее чтение из канала будет возвращать 0 (конец файла) только после потребления всех неполученных данных в канале.
EPOLLET
    Установить поведение Edge Triggered для связанного файлового дескриптора. Поведение по умолчанию для epoll равно Level Triggered. Более подробное описание архитектуры распределения событий Edge и Level Triggered смотрите в epoll(7).
 *
 */

struct sEpollEvents
{
	struct epoll_event *events;
	int count;
};

class BasicEpollServer
{
public:
    BasicEpollServer(int32_t max_eve);
	virtual ~BasicEpollServer();

	int addEvent(int fd,uint32_t mEPOLL_EVENTS);
	int removeEvent(int fd);
	sEpollEvents * wait(int32_t timeout=-1);

	static int create_fdTimer(uint32_t sec_time,uint32_t msec_time,uint32_t nsec_time);
	static int change_fdTimer(int timer_fd,uint32_t sec_time,uint32_t msec_time,uint32_t nsec_time);
	static int do_fdNONBLOCK(int fd);
private:
	int fd_epoll;
	int max_event;
	sEpollEvents cur_event;
	struct epoll_event *events; //указатель на события которые произошли
};

#endif /* EPOLLSERVER_HPP_ */
