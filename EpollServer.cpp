/*
 * EEpoll.cpp
 *
 *  Created on: 20 июл. 2019 г.
 *      Author: avatar
 */

#include "fcntl.h"
#include "EpollServer.hpp"

BasicEpollServer::BasicEpollServer(int32_t max_eve)
{
	events = new struct epoll_event[max_eve];
	fd_epoll = epoll_create(max_eve);
	max_event = max_eve;
	cur_event.events = events;
	cur_event.count = 0;

}

BasicEpollServer::~BasicEpollServer()
{
	// TODO Auto-generated destructor stub
	close(fd_epoll);
	delete events;
}

int BasicEpollServer::addEvent(int fd, uint32_t mEPOLL_EVENTS)
{
	struct epoll_event eve;
	eve.events = EPOLLIN | EPOLLOUT | EPOLLET;
	eve.data.fd = fd;
	if (epoll_ctl(fd_epoll, EPOLL_CTL_ADD, fd, &eve) == -1)
	{ //несмогли добавить
		return -1;
	}
	return 0;
}

int BasicEpollServer::removeEvent(int fd)
{
	if (epoll_ctl(fd_epoll, EPOLL_CTL_DEL, fd, NULL) == -1)
	{ //несмогли del
		return -1;
	}
	return 0;
}

sEpollEvents* BasicEpollServer::wait(int32_t timeout)
{
	cur_event.events = events;
	cur_event.count = epoll_wait(fd_epoll, events, max_event, timeout);
	if (cur_event.count == -1)
	{
		return 0;
	}
	else
		return &cur_event;
}

int BasicEpollServer::create_fdTimer(uint32_t sec_time, uint32_t msec_time,
		uint32_t nsec_time)
{
	int tfd = timerfd_create(CLOCK_MONOTONIC, 0);
	if (tfd == -1)
	{
		return -1;
	}
	struct itimerspec ts;
	ts.it_interval.tv_sec = 0;
	ts.it_interval.tv_nsec = 0;
	ts.it_value.tv_sec = sec_time;
	ts.it_value.tv_nsec = (msec_time % 1000) * 1000000 + nsec_time;

	if (timerfd_settime(tfd, 0, &ts, NULL) < 0)
	{
		close(tfd);
		return -1;
	}
	return tfd;
}

int BasicEpollServer::change_fdTimer(int timer_fd, uint32_t sec_time, uint32_t msec_time,
		uint32_t nsec_time)
{
	struct itimerspec ts;
	ts.it_interval.tv_sec = 0;
	ts.it_interval.tv_nsec = 0;
	ts.it_value.tv_sec = sec_time;
	ts.it_value.tv_nsec = (msec_time % 1000) * 1000000 + nsec_time;
	if (timerfd_settime(timer_fd, 0, &ts, NULL) < 0)
	{
//		close(timer_fd);
		return -1;
	}
	return timer_fd;
}

int BasicEpollServer::do_fdNONBLOCK(int fd)
{
	if(fcntl(fd, F_SETFL, fcntl(fd, F_GETFD, 0)|O_NONBLOCK) < 0)
	    {
	      return -1;
	    }
	  else
	    {
	     return 0;
	    }

}
